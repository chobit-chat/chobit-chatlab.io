export interface Peer {
  connection: RTCPeerConnection;
  channels: Channels;
  partnerId: string;
}

export class Invitation {
  description: RTCSessionDescription;
  offerId: string;

  constructor(args: { description: RTCSessionDescription; offerId: string }) {
    this.description = args.description;
    this.offerId = args.offerId;
  }

  toString(): string {
    return JSON.stringify(this);
  }

  static parse(code: string): Invitation {
    const { description, offerId } = JSON.parse(code);
    if (!description || !offerId) {
      throw new TypeError("Failed to parse Invitation : No properties needed");
    }
    return new Invitation({ description, offerId });
  }
}

const rtcConfiguration = {
  iceServers: [
    {
      urls: [
        "stun:stun.l.google.com:19302",
        "stun:stun1.l.google.com:19302",
        "stun:stun2.l.google.com:19302",
        "stun:stun3.l.google.com:19302",
        "stun:stun4.l.google.com:19302"
      ]
    }
  ]
};

// type ChannelType = "chat" | "greet" | "mediate";

interface Channels {
  chat: RTCDataChannel;
  greet: RTCDataChannel;
  mediate: RTCDataChannel;
}

const channelLabel: Record<keyof Channels, string> = {
  chat: "chat",
  greet: "greeting",
  mediate: "mediate"
};

function createConnection(): RTCPeerConnection {
  return new RTCPeerConnection(rtcConfiguration);
}

function createDataChannels(con: RTCPeerConnection): Channels {
  const chat = con.createDataChannel(channelLabel.chat);
  const greet = con.createDataChannel(channelLabel.greet);
  const mediate = con.createDataChannel(channelLabel.mediate);

  return {
    chat,
    greet,
    mediate
  };
}

function vanillaIcecandidate(peer: RTCPeerConnection) {
  return new Promise(resolve => {
    peer.onicecandidate = e => {
      if (!e.candidate) {
        resolve();
      }
    };
  });
}

class MediateMessage {
  invitation?: Invitation;
  mediatorId: string;
  newId: string;

  constructor(args: {
    invitation?: Invitation;
    mediatorId: string;
    newId: string;
  }) {
    this.invitation = args.invitation;
    this.mediatorId = args.mediatorId;
    this.newId = args.newId;
  }

  toString(): string {
    return JSON.stringify(this);
  }

  static parse(code: string): MediateMessage {
    const { invitation, mediatorId, newId } = JSON.parse(code);
    if (!invitation || !mediatorId || !newId) {
      throw new TypeError(
        "Failed to parse MediateMessage : No properties needed"
      );
    }
    return new MediateMessage({ invitation, mediatorId, newId });
  }
}

export class ChatRoom {
  id: string;
  peers: Peer[];
  answerWaitings: {
    newId: string;
    setAnswer: (answer: Invitation) => void;
  }[];
  onAddedMember?: (peer: Peer) => any;
  onMessage: (message: string) => any;

  constructor({
    id,
    onAddedMember,
    onMessage
  }: {
    id: string;
    onAddedMember?: (peer: Peer) => any;
    onMessage: (message: string) => any;
  }) {
    this.id = id;
    this.peers = [];
    this.answerWaitings = [];
    this.onAddedMember = onAddedMember;
    this.onMessage = onMessage;
  }

  private noticeNewPeer(newPeer: Peer) {
    this.peers.forEach(peer => {
      console.log(`send mediate message to ${peer.partnerId}`);
      const ch = peer.channels.mediate;
      if (ch.readyState === "open") {
        const message = new MediateMessage({
          mediatorId: this.id,
          newId: newPeer.partnerId
        });
        ch.send(message.toString());
      }
    });
  }

  private addNewPeer(newPeer: Peer) {
    this.noticeNewPeer(newPeer);
    this.peers.push(newPeer);
    if (this.onAddedMember) {
      this.onAddedMember(newPeer);
    }
  }

  private async sendOfferThroughMediator({
    mediatorId,
    newId
  }: MediateMessage) {
    const [offer, setAnswer] = await this.createOffer();

    const isKnown = this.peers.some(peer => peer.partnerId === newId);
    if (isKnown) {
      return;
    }

    const peer = this.peers.find(peer => peer.partnerId === mediatorId);
    if (peer == null) {
      return;
    }

    const ch = peer.channels.mediate;
    if (ch.readyState !== "open") {
      return;
    }

    const message = new MediateMessage({
      invitation: offer,
      mediatorId,
      newId
    });

    console.log(`send offer to ${newId} through ${mediatorId}`);
    ch.send(message.toString());
    this.answerWaitings.push({ newId, setAnswer });
  }

  private async sendAnswerThroughMediator({
    invitation,
    mediatorId,
    newId
  }: Required<MediateMessage>) {
    const answer = await this.createAnswer({ offer: invitation });

    const isKnown = this.peers.some(peer => peer.partnerId === newId);
    if (isKnown) {
      return;
    }

    const peer = this.peers.find(peer => peer.partnerId === mediatorId);
    if (peer == null) {
      return;
    }

    const ch = peer.channels.mediate;
    if (ch.readyState !== "open") {
      return;
    }

    const message = new MediateMessage({
      invitation: answer,
      mediatorId,
      newId
    });

    console.log(`send answer to ${invitation.offerId} through ${mediatorId}`);
    ch.send(message.toString());
  }

  private relayMediateMessage(message: Required<MediateMessage>) {
    const { invitation, newId } = message;

    const peer = (type => {
      if (type === "offer")
        return this.peers.find(peer => peer.partnerId === newId);
      if (type === "answer")
        return this.peers.find(peer => peer.partnerId === invitation.offerId);
    })(invitation.description.type);

    if (peer == null) {
      return;
    }

    const ch = peer.channels.mediate;
    if (ch.readyState !== "open") {
      return;
    }

    console.log(`relay message to ${peer.partnerId}`);

    ch.send(new MediateMessage(message).toString());
  }

  private receiveAnswerThroughMediator({
    invitation,
    newId
  }: Required<MediateMessage>) {
    const answerWaiting = this.answerWaitings.find(a => a.newId === newId);
    if (answerWaiting == null) {
      return;
    }
    answerWaiting.setAnswer(invitation);
    this.answerWaitings = this.answerWaitings.filter(a => a.newId !== newId);
  }

  private onMediateMessage(data: string) {
    const message: MediateMessage = JSON.parse(data);
    console.log(`receive MediateMessage through ${message.mediatorId}`);
    if (!message.invitation) {
      this.sendOfferThroughMediator(message);
      return;
    }
    if (message.mediatorId === this.id) {
      this.relayMediateMessage(message as Required<MediateMessage>);
      return;
    }
    if (message.invitation.description.type === "offer") {
      this.sendAnswerThroughMediator(message as Required<MediateMessage>);
      return;
    }
    if (message.invitation.description.type === "answer") {
      this.receiveAnswerThroughMediator(message as Required<MediateMessage>);
    }
  }

  async createOffer(args?: {
    onOpen: () => any;
  }): Promise<[Invitation, (answer: Invitation) => void]> {
    const connection = createConnection();
    const channels = createDataChannels(connection);

    const onGreetingMessage = (e: MessageEvent) => {
      this.addNewPeer({ connection, channels, partnerId: e.data });
      if (args) {
        args.onOpen();
      }
    };

    connection.ondatachannel = e => {
      const ch = e.channel;
      switch (ch.label) {
        case channelLabel.chat:
          ch.onmessage = e => this.onMessage(e.data);
          break;
        case channelLabel.mediate:
          ch.onmessage = e => this.onMediateMessage(e.data);
          break;
        case channelLabel.greet:
          ch.onmessage = onGreetingMessage;
      }
    };

    const offerDescription = await connection.createOffer();
    await connection.setLocalDescription(offerDescription);
    await vanillaIcecandidate(connection);

    const setAnswer = (answer: Invitation) => {
      if (answer.offerId !== this.id) {
        throw new Error(
          `answer's offerId is not mine! (answer.offerId : ${answer.offerId})`
        );
      }
      connection.setRemoteDescription(answer.description);
    };

    const offer = new Invitation({
      description: connection.localDescription!,
      offerId: this.id
    });

    return [offer, setAnswer];
  }

  async createAnswer({
    offer,
    onOpen
  }: {
    offer: Invitation;
    onOpen?: (peer: Peer) => any;
  }): Promise<Invitation> {
    const connection = createConnection();
    const channels = createDataChannels(connection);

    const newPeer: Peer = { connection, channels, partnerId: offer.offerId };

    const onOpenGreetingChannel = () => {
      channels.greet.send(this.id);
      this.addNewPeer(newPeer);
    };

    connection.ondatachannel = e => {
      const ch = e.channel;
      switch (ch.label) {
        case channelLabel.chat:
          if (onOpen) {
            ch.onopen = () => {
              if (onOpen) {
                onOpen(newPeer);
              }
            };
          }
          ch.onmessage = e => this.onMessage(e.data);
          break;
        case channelLabel.mediate:
          ch.onmessage = e => this.onMediateMessage(e.data);
          break;
        case channelLabel.greet:
          ch.onopen = onOpenGreetingChannel;
      }
    };

    await connection.setRemoteDescription(offer.description);
    const answerDescription = await connection.createAnswer();
    await connection.setLocalDescription(answerDescription);
    await vanillaIcecandidate(connection);

    const answer = new Invitation({
      description: connection.localDescription!,
      offerId: offer.offerId
    });

    return answer;
  }

  sendMessage({ message, dest }: { message: string; dest?: Peer }) {
    if (dest) {
      const ch = dest.channels.chat;
      if (ch.readyState === "open") {
        ch.send(message);
      }
      return;
    }

    this.peers.forEach(peer => this.sendMessage({ message, dest: peer }));
  }
}
