import React from "react";
import Modal from "react-modal";
import { Invitation } from "../modules/rtc";

Modal.setAppElement("#root");

type Props = {
  offerCreator: (args?: {
    onOpen: () => any;
  }) => Promise<[Invitation, (answer: Invitation) => void]>;
};

type State = {
  isOpen: boolean;
  offerCode: string;
  answerCode: string;
  answerCodeErr: string;
};

class OfferConnection extends React.Component<Props, State> {
  setAnswer: ((answer: Invitation) => void) | null;

  constructor(props: Props) {
    super(props);

    this.state = {
      isOpen: false,
      offerCode: "",
      answerCode: "",
      answerCodeErr: ""
    };

    this.setAnswer = null;
  }

  onOpen = () => this.setState({ isOpen: false });

  open = async () => {
    this.setState({ isOpen: true, answerCode: "" });
    const [offer, setAnswer] = await this.props.offerCreator({
      onOpen: this.onOpen
    });
    this.setAnswer = setAnswer;
    this.setState({ offerCode: offer.toString() });
  };

  onInputAnswerCode = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (this.setAnswer == null) {
      throw new Error("setAnswer must not be null");
    }
    const code = e.target.value;
    this.setState({ answerCode: code, answerCodeErr: "" });
    if (code === "") {
      return;
    }
    try {
      const answer = Invitation.parse(code);
      this.setAnswer(answer);
    } catch (err) {
      this.setState({ answerCodeErr: "Answerコードが不正です" });
      console.error(err);
    }
  };

  render() {
    return (
      <div>
        <button onClick={this.open}>Offer発行</button>
        <Modal isOpen={this.state.isOpen}>
          <ol>
            <li>
              <p>
                以下のOfferコードを通信相手に送ってください。
                <br />
                ※このウィンドウを閉じるとOfferコードは再生成されます。
                <br />
                その場合はOfferコードを送りなおしてください。
              </p>
              Offerコード：
              <input
                readOnly={true}
                value={this.state.offerCode}
                onFocus={e => e.target.select()}
              />
            </li>
            <li>
              <p>
                通信相手から送られてきたAnswerコードを以下のフォームにコピペしてください。
              </p>
              Answerコード：
              <input
                value={this.state.answerCode}
                onChange={this.onInputAnswerCode}
              />
              {this.state.answerCodeErr && (
                <p style={{ color: "red" }}>{this.state.answerCodeErr}</p>
              )}
            </li>
          </ol>
          <footer>
            <button onClick={() => this.setState({ isOpen: false })}>
              Cancel
            </button>
          </footer>
        </Modal>
      </div>
    );
  }
}

export default OfferConnection;
