import React, { useState } from "react";
import Modal from "react-modal";
import { Invitation } from "../modules/rtc";

Modal.setAppElement("#root");

type Props = {
  answerCreator: (args: {
    offer: Invitation;
    onOpen?: () => any;
  }) => Promise<Invitation>;
};

function AnswerConnection(props: Props) {
  const [isOpen, setIsOpen] = useState(false);
  const [offerCode, setOfferCode] = useState("");
  const [answerCode, setAnswerCode] = useState("");
  const [offerCodeErr, setOfferCodeErr] = useState("");

  const onOpen = () => setIsOpen(false);

  const open = () => {
    setOfferCode("");
    setAnswerCode("");
    setIsOpen(true);
  };

  const onInputOfferCode = async (e: React.ChangeEvent<HTMLInputElement>) => {
    const code = e.target.value;
    setOfferCode(code);
    setOfferCodeErr("");
    if (code === "") {
      return;
    }

    try {
      const offer = Invitation.parse(code);
      const answer = await props.answerCreator({ offer, onOpen });
      setAnswerCode(answer.toString());
    } catch (err) {
      setOfferCodeErr("Offerコードが不正です");
      console.error(err);
    }
  };

  return (
    <div>
      <button onClick={open}>Offer受信</button>
      <Modal isOpen={isOpen}>
        <ol>
          <li>
            <p>
              通信相手から送られてきたOfferコードを以下のフォームにコピペしてください。
            </p>
            Offerコード：
            <input value={offerCode} onChange={onInputOfferCode} />
            {offerCodeErr && <p style={{ color: "red" }}>{offerCodeErr}</p>}
          </li>
          <li>
            <p>以下のAnswerコードを通信相手に送ってください。</p>
            Answerコード：
            <input
              readOnly={true}
              value={answerCode}
              onFocus={e => e.target.select()}
            />
          </li>
        </ol>
        <footer>
          <button onClick={() => setIsOpen(false)}>Cancel</button>
        </footer>
      </Modal>
    </div>
  );
}

export default AnswerConnection;
