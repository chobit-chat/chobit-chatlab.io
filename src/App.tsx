import React from "react";
import { nanoid } from "nanoid";
import Identicon from "identicon.js";
import OfferConnection from "./components/OfferConnection";
import AnswerConnection from "./components/AnswerConnection";
import { ChatRoom, Peer } from "./modules/rtc";
import "./App.css";

type ChatTalk = {
  kind: "talk";
  name: string;
  text: string;
  icon: string;
};

type ChatInfo = {
  kind: "info";
  text: string;
};

type ChatMessage = ChatTalk | ChatInfo;

function identicon(id: string): string {
  const icon = new Identicon(id, {
    margin: 0.2,
    size: 50
  }).toString();
  return `data:image/png;base64,${icon}`;
}

type Props = {};

type State = {
  icon: string;
  name: string;
  messages: ChatMessage[];
  messageText: string;
};

class App extends React.Component<Props, State> {
  id: string;
  chatRoom: ChatRoom;

  addChatMessage = (message: ChatMessage) => {
    this.setState(state => ({ messages: [...state.messages, message] }));
  };

  sendChatMessage = (message: ChatMessage, dest?: Peer) => {
    const json = JSON.stringify(message);
    this.chatRoom.sendMessage({ message: json, dest });
  };

  onAddedMember = (peer: Peer) => {
    this.sendChatMessage(
      {
        kind: "info",
        text: `${this.state.name}が入室しました`
      },
      peer
    );
  };

  onChatMessage = (data: string) => {
    const message: ChatMessage = JSON.parse(data);
    this.addChatMessage(message);
  };

  sendTalkMessage = () => {
    const message: ChatMessage = {
      kind: "talk",
      name: this.state.name,
      text: this.state.messageText,
      icon: this.state.icon
    };
    this.addChatMessage(message);
    this.sendChatMessage(message);
  };

  constructor(props: Props) {
    super(props);

    const id = nanoid();

    this.id = id;

    this.chatRoom = new ChatRoom({
      id,
      onAddedMember: this.onAddedMember,
      onMessage: this.onChatMessage
    });

    this.state = {
      icon: identicon(id),
      name: id,
      messages: [],
      messageText: ""
    };
  }

  render() {
    return (
      <React.Fragment>
        <header>
          <div>
            <img className="icon" src={this.state.icon} alt="" />
            <input
              className="username"
              value={this.state.name}
              onChange={e => this.setState({ name: e.target.value })}
            />
          </div>

          <div>
            <OfferConnection
              offerCreator={args => this.chatRoom.createOffer(args)}
            />
            <AnswerConnection
              answerCreator={args => this.chatRoom.createAnswer(args)}
            />
          </div>
        </header>
        <div>
          <input
            type="text"
            value={this.state.messageText}
            onChange={e => this.setState({ messageText: e.target.value })}
          />
          <button onClick={this.sendTalkMessage}>送信</button>
        </div>
        <ul>
          {this.state.messages.map((message, i) => {
            switch (message.kind) {
              case "talk":
                return (
                  <li className={`chat-${message.kind}`} key={`message-${i}`}>
                    <img className="icon" src={message.icon} alt="" />
                    <div>{message.name}</div>
                    <div>{message.text}</div>
                  </li>
                );
              case "info":
                return (
                  <li className={`chat-${message.kind}`} key={`message-${i}`}>
                    <div>{message.text}</div>
                  </li>
                );
              default:
                return ((_: never): never => {
                  throw new Error("Unexpected value. Should have been never.");
                })(message);
            }
          })}
        </ul>
      </React.Fragment>
    );
  }
}

export default App;
